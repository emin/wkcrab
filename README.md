# WKCrab
Anki 2 addon to import all your WaniKani burned items to Anki.
And by the way, the name is a pun on WKGrab. Terrible, I know.

## TODO
The addon is not complete. The first review for all imported cards must be
schedule for ~5 months in the future. Currently they are added as brand new.

## Quick Tutorial
### Installing
Just download wkcrab.py and drop it into Anki's addons directory.
If you want WKCrab to colorize radicals represented by images, you need PIL
too. On Linux you can probably get it from your distro's software repository.
I have no idea about other OSes.

### Configuring
Open wkcrab.py with any standard text editor and look for the line that says
`API_KEY`. You can set your API key there by replacing the default one. Make
sure not to remove the quotes.
Right below that you can set the name of the deck you wish to use, and below
that the default color for radicals represented by images (in rgb, 0 to 255
format; e.g. (255, 0, 0) is pure red).
All of these can also be specified on Anki later. Setting them here just
makes thigs faster.

### Using
Open up Anki after installing the addon. If it was already open, reopen it.
A new option should be available in the Tools menu. Click it.
If you configured the addon, your API key should already be in the textbox;
if not, type it now. Same for the name of the deck you wish to use.
If you have PIL installed, you may also choose a color to colorize radicals
represented by images.
If the given deck doesn't exist, you will be asked if you want to create it.
The API key will be checked to make sure you typed it correctly. If you did,
your username should appear. Confirm everything is OK and wait. The process
usually takes a couple of seconds.
Once it's done all burned items in your WaniKani account will be in your deck.
You may then repeat the process every time you burn new items to update it.

And that's it.

## Thanks
Some stuff is stolen from <https://github.com/nigelkerr/wanikani2anki> ;).
